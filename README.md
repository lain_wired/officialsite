# 公式サイト
アドレス
---
https://lain-wired.net/

Special Thanks
---
+ [AKIRA-MIYAKE](https://github.com/AKIRA-MIYAKE/LoveLetterTypewriterTTL)さん
    + `LoveLetterTypewriterTTL/`を拝借

注意事項
---
+ `js/miner.js`
    + 公開鍵を自分のものに変更すること

## License
GNU GPLv2
